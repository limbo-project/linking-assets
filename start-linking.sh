#!/bin/bash
#@todo to datasetincludes
host="http://localhost:8080"
#host="http://limbolimes.cs.upb.de:8080"
results="./results"
linkspecs="./linkspecs"
mkdir -p "${results}"
mkdir -p "${linkspecs}"
while read l1; do
    x1=`echo "${l1}" | cut -d '/' -f 5`
    while read l2; do
        x2=`echo "${l2}" | cut -d '/' -f 5`
        cat template.ttl | sed -e "s|?LIMBOSOURCE|${l1}|" -e "s|?LIMBOTARGET|${l2}|" > "${linkspecs}/ls-${x1}-${x2}.ttl"
        fin=0
        rid=`curl -F config_file=@"${linkspecs}/ls-${x1}-${x2}.ttl" "${host}/submit" 2>/dev/null | jq '.requestId' | cut -d '"' -f 2`
        while [[ fin -le 1 ]]; do
            fin=`curl "${host}/status/${rid}" 2>/dev/null | jq .status.code`
            sleep 0.5;
        done
        curl "${host}/result/${rid}/accept.nt" 2>/dev/null > tmpres
        size=`wc -l <tmpres | bc`
        [[ $size -ge 1 ]] && cp tmpres "${results}/result-${x1}-${x2}.nt"
        rm tmpres
    done <datasets
done <datasets
